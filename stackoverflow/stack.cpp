#include <algorithm>
#include <cstddef>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>

const char* stack_hi=0;
const char* stack_lo=0;
void stackmon();

template <int N> struct agg {
  agg<N-1> _inner;
  int _mine[256];
  
  agg()
    :_inner()
  {
    std::fill(&_mine[0],&_mine[256],N+1);
    stackmon();
  }

  agg(const agg<N-1>& i)
    :_inner(i)
  {
    std::fill(&_mine[0],&_mine[256],N+1);
    stackmon();
  }

  ~agg()
  {
    stackmon();
  }
};

template <> struct agg<0> {
  int _mine[256];
  
  agg()
  {
    std::fill(&_mine[0],&_mine[256],1);
    stackmon();
  }

  ~agg()
  {
    stackmon();
  }
};

void test(const std::function<size_t()>&,const std::string& qualifier="");
size_t test0();
size_t test1();
size_t test2();
size_t test3();
size_t test4();
size_t test5();
size_t test6();
size_t test7();
size_t test8();
size_t test9();
size_t test10();
size_t test11();
size_t test12();
size_t test13();
size_t test14();
size_t test15();
size_t test15_var();
size_t test15_fn();
size_t test15_fnn();
size_t test15_fn2();
size_t test15_fn2n();

#ifndef NOLAMBDA
size_t test15_l();
size_t test15_l2();
#endif

int main(int,char**) {
  test(&test0);
  test(&test1);
  test(&test2);
  test(&test3);
  test(&test4);
  test(&test5);
  test(&test6);
  test(&test7);  
  test(&test8);  
  test(&test9);  
  test(&test10);  
  test(&test11);  
  test(&test12);  
  test(&test13);  
  test(&test14);  
  test(&test15);  
  test(&test15_var,"var");  
  test(&test15_fn,"f");  
  test(&test15_fnn,"fn");  
  test(&test15_fn2,"fn2");  
  test(&test15_fn2n,"fn2n");  
#ifndef NOLAMBDA
  test(&test15_l,"l");  
  test(&test15_l2,"l2");
#endif
  return 0;
}

void test(const std::function<size_t()>& fn,const std::string& qualifier) {
  stack_hi=0;
  stackmon();
  const size_t s=fn();
  std::cout << s/1024.0 << qualifier << "," << (stack_hi-stack_lo)/1024.0 << std::endl;
}

// ------

size_t test0() {
  agg<0> a=agg<0>();
  stackmon();
  return sizeof(a);
}

// ------

size_t test1() {
  agg<1> a=agg<1>(
    agg<0>()
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test2() {
  agg<2> a=agg<2>(
    agg<1>(
      agg<0>()
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test3() {
  agg<3> a=agg<3>(
    agg<2>(
      agg<1>(
	agg<0>()
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test4() {
  agg<4> a=agg<4>(
    agg<3>(
      agg<2>(
	agg<1>(
	  agg<0>()
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test5() {
  agg<5> a=agg<5>(
    agg<4>(
      agg<3>(
	agg<2>(
	  agg<1>(
	    agg<0>()
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test6() {
  agg<6> a=agg<6>(
    agg<5>(
      agg<4>(
	agg<3>(
	  agg<2>(
	    agg<1>(
	      agg<0>()
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test7() {
  agg<7> a=agg<7>(
    agg<6>(
      agg<5>(
	agg<4>(
	  agg<3>(
	    agg<2>(
	      agg<1>(
		agg<0>()
	      )
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test8() {
  agg<8> a=agg<8>(
    agg<7>(
      agg<6>(
	agg<5>(
	  agg<4>(
	    agg<3>(
	      agg<2>(
		agg<1>(
		  agg<0>()
		)
	      )
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test9() {
  agg<9> a=agg<9>(
    agg<8>(
      agg<7>(
	agg<6>(
	  agg<5>(
	    agg<4>(
	      agg<3>(
		agg<2>(
		  agg<1>(
		    agg<0>()
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test10() {
  agg<10> a=agg<10>(
    agg<9>(
      agg<8>(
	agg<7>(
	  agg<6>(
	    agg<5>(
	      agg<4>(
		agg<3>(
		  agg<2>(
		    agg<1>(
		      agg<0>()
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test11() {
  agg<11> a=agg<11>(
    agg<10>(
      agg<9>(
	agg<8>(
	  agg<7>(
	    agg<6>(
	      agg<5>(
		agg<4>(
		  agg<3>(
		    agg<2>(
		      agg<1>(
			agg<0>()
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test12() {
  agg<12> a=agg<12>(
    agg<11>(
      agg<10>(
	agg<9>(
	  agg<8>(
	    agg<7>(
	      agg<6>(
		agg<5>(
		  agg<4>(
		    agg<3>(
		      agg<2>(
			agg<1>(
			  agg<0>()
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test13() {
  agg<13> a=agg<13>(
    agg<12>(
      agg<11>(
	agg<10>(
	  agg<9>(
	    agg<8>(
	      agg<7>(
		agg<6>(
		  agg<5>(
		    agg<4>(
		      agg<3>(
			agg<2>(
			  agg<1>(
			    agg<0>()
			  )
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test14() {
  agg<14> a=agg<14>(
    agg<13>(
      agg<12>(
	agg<11>(
	  agg<10>(
	    agg<9>(
	      agg<8>(
		agg<7>(
		  agg<6>(
		    agg<5>(
		      agg<4>(
			agg<3>(
			  agg<2>(
			    agg<1>(
			      agg<0>()
			    )
			  )
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

size_t test15() {
  agg<15> a=agg<15>(
    agg<14>(
      agg<13>(
	agg<12>(
	  agg<11>(
	    agg<10>(
	      agg<9>(
		agg<8>(
		  agg<7>(
		    agg<6>(
		      agg<5>(
			agg<4>(
			  agg<3>(
			    agg<2>(
			      agg<1>(
				agg<0>()
			      )
			    )
			  )
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

// Variant: try breaking nesting with a variable
size_t test15_var() {

  agg<12> a12=agg<12>(
    agg<11>(
      agg<10>(
	agg<9>(
	  agg<8>(
	    agg<7>(
	      agg<6>(
		agg<5>(
		  agg<4>(
		    agg<3>(
		      agg<2>(
			agg<1>(
			  agg<0>()
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );

  agg<15> a=agg<15>(
    agg<14>(
      agg<13>(
	a12
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

// Variant: try breaking nesting with a helper function
agg<12> test15_fn_helper();

size_t test15_fn() {

  agg<15> a=agg<15>(
    agg<14>(
      agg<13>(
	test15_fn_helper()
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

agg<12> test15_fn_helper() {
  return agg<12>(
    agg<11>(
      agg<10>(
	agg<9>(
	  agg<8>(
	    agg<7>(
	      agg<6>(
		agg<5>(
		  agg<4>(
		    agg<3>(
		      agg<2>(
			agg<1>(
			  agg<0>()
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
}

// ------

// Variant: try breaking nesting with a noninline helper function
agg<12> test15_fnn_helper() __attribute__((noinline));

size_t test15_fnn() {

  agg<15> a=agg<15>(
    agg<14>(
      agg<13>(
	test15_fnn_helper()
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

agg<12> test15_fnn_helper() {
  return agg<12>(
    agg<11>(
      agg<10>(
	agg<9>(
	  agg<8>(
	    agg<7>(
	      agg<6>(
		agg<5>(
		  agg<4>(
		    agg<3>(
		      agg<2>(
			agg<1>(
			  agg<0>()
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
}


// ------

// Variant: try breaking nesting with 2 helper functions
agg<12> test15_fn2_helper0();
agg<15> test15_fn2_helper1(const agg<12>&);

size_t test15_fn2() {  
  agg<15> a=test15_fn2_helper1(test15_fn2_helper0());
  stackmon();
  return sizeof(a);
}

agg<12> test15_fn2_helper0() {
  return agg<12>(
    agg<11>(
      agg<10>(
	agg<9>(
	  agg<8>(
	    agg<7>(
	      agg<6>(
		agg<5>(
		  agg<4>(
		    agg<3>(
		      agg<2>(
			agg<1>(
			  agg<0>()
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
}

agg<15> test15_fn2_helper1(const agg<12>& a12) {
  return agg<15>(
    agg<14>(
      agg<13>(
	a12
      )
    )
  );
}

// ------

// Variant: try breaking nesting with 2 noninline helper functions
agg<12> test15_fn2n_helper0() __attribute__((noinline));
agg<15> test15_fn2n_helper1(const agg<12>&) __attribute__((noinline));

size_t test15_fn2n() {  
  agg<15> a=test15_fn2n_helper1(test15_fn2n_helper0());
  stackmon();
  return sizeof(a);
}

agg<12> test15_fn2n_helper0() {
  return agg<12>(
    agg<11>(
      agg<10>(
	agg<9>(
	  agg<8>(
	    agg<7>(
	      agg<6>(
		agg<5>(
		  agg<4>(
		    agg<3>(
		      agg<2>(
			agg<1>(
			  agg<0>()
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    )
  );
}

agg<15> test15_fn2n_helper1(const agg<12>& a12) {
  return agg<15>(
    agg<14>(
      agg<13>(
	a12
      )
    )
  );
}

// ------
#ifndef NOLAMBDA

// Variant: try breaking nesting with lambda function

size_t test15_l() {

  auto helper = []() {
    return agg<12>(
      agg<11>(
	agg<10>(
	  agg<9>(
	    agg<8>(
	      agg<7>(
		agg<6>(
		  agg<5>(
		    agg<4>(
		      agg<3>(
			agg<2>(
			  agg<1>(
			    agg<0>()
			  )
			)
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    );
  };

  agg<15> a=agg<15>(
    agg<14>(
      agg<13>(
	helper()
      )
    )
  );
  stackmon();
  return sizeof(a);  
}

// ------

// Variant: try breaking nesting with 2 lambda functions

size_t test15_l2() {

  auto helper0 = []() {
    return agg<10>(
      agg<9>(
	agg<8>(
	  agg<7>(
	    agg<6>(
	      agg<5>(
		agg<4>(
		  agg<3>(
		    agg<2>(
		      agg<1>(
			agg<0>()
		      )
		    )
		  )
		)
	      )
	    )
	  )
	)
      )
    );
  };

  auto helper1 = [](const agg<10>& a10) {
    return agg<15>(
      agg<14>(
	agg<13>(
	  agg<12>(
	    agg<11>(
	      a10
	    )
	  )
	)
      )
    );
  };

  agg<15> a=helper1(helper0());
  stackmon();
  return sizeof(a);  
}

#endif

// ------

void stackmon() {
  const char p=0;
  if (stack_hi==0) {
    stack_hi=&p;
    stack_lo=stack_hi;
  } else {
    stack_lo=std::min(&p,stack_lo);
  }
}

#!/usr/bin/env python

import csv
import matplotlib.pyplot as plt
import numpy as np
import sys

nplots=len(sys.argv)-1
n=0

for filename in sys.argv[1:]:
    with open(filename,'r') as csvfile:

        x=[]
        y=[]
        reader=csv.reader(csvfile)
        for row in reader:
            x.append(row[0])
            y.append(float(row[1]))

        xpos=np.arange(len(x))

        n+=1
        plt.subplot(2,(1+nplots)/2,n)
        w=0.8
        plt.bar(xpos,np.array(y),w)
        plt.xticks(xpos+0.5*w,x,rotation=-90)
        plt.title('Results: '+filename)
        plt.xlabel('Testcase (aggregated KByte)')
        plt.ylabel('Peak stack usage (KByte)')

plt.subplots_adjust(hspace=0.5)
plt.show()

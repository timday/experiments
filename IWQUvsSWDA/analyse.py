#!/usr/bin/env python

# Posted to https://moneyforums.citywire.co.uk/yaf_postsm62591_What-do-you-take--Quality--companies-to-mean.aspx#post62591

import csv
import matplotlib.pyplot as plt

ignoredTickers=set(['AUD','CAD','CHF','DKK','EUR','GBP','HKD','ILS','JPY','NOK','NZD','SEK','SGD','USD','MARGIN_EUR','-'])

def main():

    tickerToName={}
    tickerToSWDAWeight={}
    with open('SWDA_holdings-20180517.csv') as csvfile:
        reader=csv.reader(csvfile)
        reader.next()
        reader.next()
        reader.next()
        for row in reader:
            if len(row)>=13:
                if not row[0] in ignoredTickers:
                    ticker='{} ({}, {})'.format(row[0],row[11],row[12])
                    if ticker in tickerToName:
                        raise ValueError('Duplicate ticker "{}"'.format(ticker))
                    tickerToName[ticker]=row[1]
                    tickerToSWDAWeight[ticker]=float(row[3])

    print 'SWDA: {} holdings, total weight {}'.format(len(tickerToSWDAWeight),sum(x for x in tickerToSWDAWeight.values()))

    tickerToIWQUWeight={}
    with open('IWQU_holdings-20180517.csv') as csvfile:
        reader=csv.reader(csvfile)
        reader.next()
        reader.next()
        reader.next()
        for row in reader:
            if len(row)>=13:
                if not row[0] in ignoredTickers:
                    ticker='{} ({}, {})'.format(row[0],row[11],row[12])
                    if not ticker in tickerToName:
                        print 'Unexpected ticker {} in IWQU holdings (weight {})'.format(ticker,row[3])
                    else:
                        tickerToIWQUWeight[ticker]=float(row[3])

    print 'IWQU: {} holdings, total weight {}'.format(len(tickerToIWQUWeight),sum(x for x in tickerToIWQUWeight.values()))

    print 'IWQU holdings {:.1f}% of SWDA'.format(sum(tickerToSWDAWeight[ticker] for ticker in tickerToIWQUWeight.keys()))
    print

    tickerToFactor={}
    for ticker in tickerToIWQUWeight.keys():
        if tickerToSWDAWeight[ticker]>0.0:
            tickerToFactor[ticker]=tickerToIWQUWeight[ticker]/tickerToSWDAWeight[ticker]
        else:
            tickerToFactor[ticker]=1.0

    print 'Top weighted IWQU stock factors'
    for it in sorted(tickerToIWQUWeight.keys(),key=lambda x: tickerToIWQUWeight[x],reverse=True)[:20]:
        print '{:40s}: {:30s}: {:>4.1f}'.format(tickerToName[it],it,tickerToFactor[it])
    print

    print 'All IWQU tickers, ranked by factor'
    for it in sorted(tickerToFactor.items(),key=lambda x: x[1],reverse=True):
        print '{:40s}: {:30s}: {:>4.1f}'.format(tickerToName[it[0]],it[0],it[1])
    print

    xs=[tickerToSWDAWeight[ticker] for ticker in tickerToIWQUWeight.keys()]
    ys=[tickerToIWQUWeight[ticker] for ticker in tickerToIWQUWeight.keys()]
    cs=[tickerToFactor[ticker] for ticker in tickerToIWQUWeight.keys()]

    top10SWDA=[x[0] for x in sorted(tickerToSWDAWeight.items(),key=lambda x: x[1],reverse=True)][:10]
    top10IWQU=[x[0] for x in sorted(tickerToIWQUWeight.items(),key=lambda x: x[1],reverse=True)][:10]
    top10Factor=[x[0] for x in sorted(tickerToFactor.items(),key=lambda x: x[1],reverse=True)][:10]
    tops=set(top10SWDA).union(set(top10IWQU)).union(set(top10Factor))

    for ticker in sorted(list(tops),key=lambda x: tickerToSWDAWeight[x],reverse=True):
        print '{:40s}: {:30s}: {:>4.1f}'.format(tickerToName[ticker],ticker,tickerToSWDAWeight[ticker])
        
    txts=[(ticker.split('(')[0].strip(),tickerToName[ticker],tickerToSWDAWeight[ticker]) if ticker in tops else None for ticker in tickerToIWQUWeight.keys()]
    plt.plot([0.0,10.0],[0.0,10.0],color='#888888')
    plt.scatter(xs,ys,c=cs,edgecolors='none',cmap=plt.get_cmap('rainbow'))
    plt.colorbar()
    infos=[]
    for i in xrange(len(txts)):
        if txts[i]!=None:
            plt.gca().text(xs[i],ys[i],txts[i][0],{'ha':'left','va':'bottom'},rotation=45.0)
            infos.append(txts[i])
    info='\n'.join('{}: {}'.format(it[0],it[1]) for it in sorted(infos,key=lambda x: x[2],reverse=True))
    print
    print 'Labelling interesting tickers:'
    print info
    plt.xscale('symlog',linthreshx=0.1)
    plt.yscale('symlog',linthreshy=0.1)
    plt.xlabel('SWDA weighting % (linear below 0.1%)')
    plt.ylabel('IWQU weighting % (linear below 0.1%)')
    plt.xlim(xmin=0,xmax=max(10.0,max(xs)+1.0))
    plt.ylim(ymin=0,ymax=max(10.0,max(ys)+1.0))
    plt.title('IWQU weighting vs. SWDA weighting\nColoured by factor')
    plt.text(plt.gca().get_xlim()[1]*0.1,0.01,info,ha='left',va='bottom',size='xx-small')
    plt.show()
        
if __name__ == '__main__':
    main()

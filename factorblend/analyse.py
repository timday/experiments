#!/usr/bin/env python

# Using
# MSCI World Minimum Volatility
# MSCI World Value Factor
# MSCI World Quality Factor
# MSCI World Momentum Factor
# MSCI World Size Factor
# (But don't include the MSCI World Multifactor one.)
# try to fit
# MSCI World

# Posted at https://moneyforums.citywire.co.uk/yaf_postsm80607_Smart-beta-and-factor-correlation.aspx#post80607

import collections
import csv
import numpy as np

ignoredTickers=frozenset(['AUD','CAD','CHF','DKK','EUR','GBP','HKD','ILS','JPY','NOK','NZD','SEK','SGD','USD','MARGIN_EUR','-'])

def readHoldings(filename):
    tickerToName={}
    holdings={}
    with open(filename) as csvfile:
        reader=csv.reader(csvfile)
        reader.next()
        reader.next()
        reader.next()
        for row in reader:
            if len(row)>=13:
                if not row[0] in ignoredTickers:
                    ticker='{} ({}, {})'.format(row[0],row[11],row[12])
                    if ticker in tickerToName:
                        raise ValueError('Duplicate ticker "{}"'.format(ticker))
                    tickerToName[ticker]=row[1]
                    if float(row[3])!=0.0:
                        holdings[ticker]=float(row[3])/100.0
    t=sum(x for x in holdings.values())
    print '{}: {} non-zero non-cash holdings, total weight {}'.format(filename,len(holdings),t)
    normalizedHoldings=collections.defaultdict(float,{k:v/t for k,v in holdings.items()})
    return normalizedHoldings,tickerToName

def main():
    SWDA,tickerToName=readHoldings('SWDA_holdings.csv')
    IWMO=readHoldings('IWMO_holdings.csv')[0]
    IWQU=readHoldings('IWQU_holdings.csv')[0]
    IWSZ=readHoldings('IWSZ_holdings.csv')[0]
    IWVL=readHoldings('IWVL_holdings.csv')[0]
    MVOL=readHoldings('MVOL_holdings.csv')[0]
    print
    
    tickers=[x[0] for x in sorted(SWDA.items(),key= lambda x: x[1],reverse=True)]
    vSWDA=np.array([SWDA[x] for x in tickers])

    components=[IWMO,IWQU,IWSZ,IWVL,MVOL]
    componentsName=['IWMO','IWQU','IWSZ','IWVL','MVOL']
    componentsArray=np.array([np.array([y[x] for x in tickers]) for y in components])

    available=frozenset(reduce(lambda x,y: x.union(set(y.keys())),components,set()))
    print '{} available holdings'.format(len(available))
    missing=frozenset(SWDA.keys()).difference(available)
    print '{} unavailable holdings, total SWDA weight {}'.format(len(missing),sum([SWDA[x] for x in missing]))
    print

    fit=np.linalg.lstsq(componentsArray.T,vSWDA)[0]
    print 'Coefficients of best fit to SWDA:'
    for i in xrange(len(components)):
        print '  {}: {:>5.1%}'.format(componentsName[i],fit[i])
    print 'Total coefficients:  {:>5.1%}'.format(sum(fit))
    print 'Total weights:       {:>5.1%}'.format(sum(sum([fit[i]*components[i][x] for i in xrange(len(components))]) for x in tickers))
    print

    print 'Blended result:'
    print 'Ticker                         Name                          Target   Blend   Diff'
    for x in tickers[:40]:
        tgt=SWDA[x]
        mix=fit[0]*IWMO[x]+fit[1]*IWQU[x]+fit[2]*IWSZ[x]+fit[3]*IWVL[x]+fit[4]*MVOL[x]
        print '{:30s} {:30s} {:>5.1%}   {:>5.1%}   {:>5.1%}'.format(x,tickerToName[x],tgt,mix,mix-tgt)

    print'--------------------'
        
    # But... coefficients don't sum to one.
    # What if want to optimize difference between vSWDA and fit[0,1,2,3]*[IWMO,IWQU,IWSZ,IWVL]+(1-sum(fit(0,1,2,3)))*MVOL
    # =fit[0,1,2,3]*[IWMO-MVOL,IWQU-MVOL,IWSZ-MVOL,IWVL-MVOL]+MVOL
    # So solve SWDA-MVOL = fit[0,1,2,3]*[IWMO-MVOL,IWQU-MVOL,IWSZ-MVOL,IWVL-MVOL]

    xvSWDA=vSWDA-componentsArray[4]
    xComponentsArray=componentsArray[:4]-np.array([componentsArray[4],componentsArray[4],componentsArray[4],componentsArray[4]])
    xFit=np.linalg.lstsq(xComponentsArray.T,xvSWDA)[0]
    print 'Coefficients of constrained best fit to SWDA:'
    for i in xrange(len(xFit)):
        print '  {}: {:>5.1%}'.format(componentsName[i],xFit[i])
    print 'Total coefficients:  {:>5.1%}'.format(sum(xFit))
    print 'Implicit MVOL coefficient: {:>5.1%}'.format(1.0-sum(xFit))
    xFit=np.append(xFit,[1.0-sum(xFit)])
    print 'Total weights:       {:>5.1%}'.format(sum(sum([xFit[i]*components[i][x] for i in xrange(len(components))]) for x in tickers))

    print 'Blended result:'
    print 'Ticker                         Name                          Target   Blend   Diff'
    for x in tickers[:40]:
        tgt=SWDA[x]
        mix=xFit[0]*IWMO[x]+xFit[1]*IWQU[x]+xFit[2]*IWSZ[x]+xFit[3]*IWVL[x]+xFit[4]*MVOL[x]
        print '{:30s} {:30s} {:>5.1%}   {:>5.1%}   {:>5.1%}'.format(x,tickerToName[x],tgt,mix,mix-tgt)

if __name__ == '__main__':
    main()

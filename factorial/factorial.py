#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np

# Multiplity of prime factor p in n!
def fmultiplicity(p,n):
    r=0
    q=p
    while q <= n:
        r += n/q
        q *= p
    return r

n=np.arange(1,10000)
m5=map(lambda x: fmultiplicity(5,x),n)
plt.plot(n,n/4,label='n/4 estimate')
plt.plot(n,m5,label='Legendre formula')
plt.legend(loc=4)
plt.xlabel('n')
plt.title('Multiplicity of prime factor 5 in n!')

n=np.arange(1,1000)
m5=map(lambda x: fmultiplicity(5,x),n)
plt.figure()
plt.plot(n,m5-n/4)
plt.yticks(np.arange(0,-5,-1))
plt.title('Multiplicity of prime factor 5 in n!, deviation from n/4')
plt.show()

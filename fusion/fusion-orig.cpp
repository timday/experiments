#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Test
        
// Add any additional #includes from Boost or the standard library that you require.
#include <boost/fusion/container/vector.hpp>
#include <boost/test/unit_test.hpp>
#include <vector>
        
namespace fusion = boost::fusion;

template <typename T, int Max> void a( T &t, unsigned int n )
{
  // task1: set elements of 't' to successive primes, starting with the 'n'th
  // See 'test1' below
}
        
template <typename T, int Max> void b( T &t, unsigned int n )
{       
  // task2: add 'n' entries to container 't', so each row contains successive primes
  // See 'test2' below
}
        
/*
 * Other than uncommenting tests, change nothing below this line
 */
        
// test1
        
BOOST_AUTO_TEST_CASE( test1 )
{
  typedef fusion::vector<int, double> row;
        
  row r;
        
  a<row, 2>( r, 1 ); // set to primes, starting from 1st prime
  //  BOOST_CHECK_EQUAL( fusion::at_c<0>( r ), 2 );
  //  BOOST_CHECK_EQUAL( fusion::at_c<1>( r ), 3.0 );
        
  a<row, 2>( r, 6 ); // set to primes, starting from 6th prime
  //  BOOST_CHECK_EQUAL( fusion::at_c<0>( r ), 13 );
  //  BOOST_CHECK_EQUAL( fusion::at_c<1>( r ), 17.0 );
}
        
// test2
        
BOOST_AUTO_TEST_CASE( test2 )
{
  typedef fusion::vector<int, double, std::string> row;
  typedef std::vector<row> vec;
        
  vec v;
  b<vec, 3>( v, 20 );     // create 20 rows, each row containing successive primes
        
  //  BOOST_REQUIRE_EQUAL( v.size(), 20u );
        
  vec::const_iterator it( v.begin() ), endit( v.end() );
  //  BOOST_CHECK_EQUAL( fusion::at_c<0>( *it ), 2 );
  //  BOOST_CHECK_EQUAL( fusion::at_c<1>( *it ), 3.0 );
  //  BOOST_CHECK_EQUAL( fusion::at_c<2>( *it ), "5" );
        
  ++it;
        
  //  BOOST_CHECK_EQUAL( fusion::at_c<0>( *it ), 7 );
  //  BOOST_CHECK_EQUAL( fusion::at_c<1>( *it ), 11.0 );
  //  BOOST_CHECK_EQUAL( fusion::at_c<2>( *it ), "13" );
        
  /* ... */
        
  //  BOOST_CHECK_EQUAL( fusion::at_c<0>( v.back() ), 271 );
  //  BOOST_CHECK_EQUAL( fusion::at_c<1>( v.back() ), 277.0 );
  //  BOOST_CHECK_EQUAL( fusion::at_c<2>( v.back() ), "281" );
}

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Test
        
// Add any additional #includes from Boost or the standard library that you require.
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/container/vector.hpp>
#include <boost/fusion/sequence/intrinsic.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/test/test_tools.hpp> 
#include <boost/test/unit_test.hpp>
#include <vector>

namespace fusion = boost::fusion;

// Class which yields the sequence of primes.
// Slight abuse of const/mutable due to boost::fusion::for_each wanting to invoke operator(...) on const objects.
// Can imagine moving known primes out to some singleton state so repeated iterations over primes reuse previously computed results.
struct Primes
{
  Primes()
  {}

  // Draw next prime and return it
  // Could be more efficient: e.g sieving / limiting search to x*x<i ...
  // ...that would be overkill for ranges involved though; keep it simple.
  int operator()() const
  {
    // Identify next candidate value
    int i=(known_.empty() ? 1 : known_.back())+1;

    // Search from next candidate for a value which has no known prime as a factor
    while (has_known_prime_as_factor_(i)) ++i;

    // Found one
    known_.push_back(i);
    return i;
  }

  // Draw next prime and assign to dst in some sensible way.
  // boost::lexical_cast seems reasonable for test cases given.
  template <typename T> void operator()(T& dst) const {
    dst=boost::lexical_cast<T>((*this)());
  }

private:

  bool has_known_prime_as_factor_(int i) const {
    return (
      std::find_if(
	known_.begin(),
	known_.end(),
	[i](int x) {return (i%x==0);} 
      )
      !=
      known_.end()
    );
  }

  mutable std::vector<int> known_;
};

// Check the above does what's expected
BOOST_AUTO_TEST_CASE( primes ) {
  Primes primes;
  BOOST_CHECK_EQUAL(primes(),2);
  BOOST_CHECK_EQUAL(primes(),3);
  BOOST_CHECK_EQUAL(primes(),5);
  BOOST_CHECK_EQUAL(primes(),7);
  BOOST_CHECK_EQUAL(primes(),11);
  BOOST_CHECK_EQUAL(primes(),13);

  float next=0.0f;
  primes(next);
  BOOST_CHECK_EQUAL(next,17.0f);
}

template <typename T, int Max> void a( T &t, unsigned int n )
{
  // task1: set elements of 't' to successive primes, starting with the 'n'th
  // See 'test1' below

  Primes primes;
  for (unsigned int i=1;i<n;++i) primes();  // Draw and discard unneeded primes.

  fusion::for_each(t,primes);
}
        
template <typename T, int Max> void b( T &t, unsigned int n )
{       
  // task2: add 'n' entries to container 't', so each row contains successive primes
  // See 'test2' below

  Primes primes;

  // Comment: hard to know what it's reasonable to assume about T.
  // This meets test's requirements.
  t.resize(n);

  std::for_each(
    t.begin(),
    t.end(),
    [&primes](typename T::value_type& row) {fusion::for_each(row,primes);}
  );
}
        
/*
 * Other than uncommenting tests, change nothing below this line
 */
        
// test1
        
BOOST_AUTO_TEST_CASE( test1 )
{
  typedef fusion::vector<int, double> row;
        
  row r;
        
  a<row, 2>( r, 1 ); // set to primes, starting from 1st prime
  BOOST_CHECK_EQUAL( fusion::at_c<0>( r ), 2 );
  BOOST_CHECK_EQUAL( fusion::at_c<1>( r ), 3.0 );
        
  a<row, 2>( r, 6 ); // set to primes, starting from 6th prime
  BOOST_CHECK_EQUAL( fusion::at_c<0>( r ), 13 );
  BOOST_CHECK_EQUAL( fusion::at_c<1>( r ), 17.0 );
}
        
// test2
        
BOOST_AUTO_TEST_CASE( test2 )
{
  typedef fusion::vector<int, double, std::string> row;
  typedef std::vector<row> vec;
        
  vec v;
  b<vec, 3>( v, 20 );     // create 20 rows, each row containing successive primes
        
  BOOST_REQUIRE_EQUAL( v.size(), 20u );
        
  vec::const_iterator it( v.begin() ), endit( v.end() );
  BOOST_CHECK_EQUAL( fusion::at_c<0>( *it ), 2 );
  BOOST_CHECK_EQUAL( fusion::at_c<1>( *it ), 3.0 );
  BOOST_CHECK_EQUAL( fusion::at_c<2>( *it ), "5" );
        
  ++it;
        
  BOOST_CHECK_EQUAL( fusion::at_c<0>( *it ), 7 );
  BOOST_CHECK_EQUAL( fusion::at_c<1>( *it ), 11.0 );
  BOOST_CHECK_EQUAL( fusion::at_c<2>( *it ), "13" );
        
  /* ... */
        
  BOOST_CHECK_EQUAL( fusion::at_c<0>( v.back() ), 271 );
  BOOST_CHECK_EQUAL( fusion::at_c<1>( v.back() ), 277.0 );
  BOOST_CHECK_EQUAL( fusion::at_c<2>( v.back() ), "281" );
}

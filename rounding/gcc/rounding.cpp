// Question: Is there a rounding mode such that (x-1.0f)+1.0f
// is a lossless transformation for all floats ?

#include <iostream>
#include <xmmintrin.h>
#include <emmintrin.h>

using namespace std;

void test(int mode,const char* modename) {

  cout << "Testing " << modename << "..." << endl;
  _MM_SET_ROUNDING_MODE(mode);

  for (long long int i=0;i<=(1LL<<32);++i) {
    float x=i;
    if (((x-1.0f)+1.0f) != x) {
      cout.precision(1);
      cout << fixed << "...found " << x << " :" << endl;
      cout << x << ", -1.0f -> " << x-1.0f << ", +1.0f -> " << (x-1.0f)+1.0f << endl << endl;
      return;
    }
  }
  cout << "...seemed to be fine over range tested" << endl << endl;
}

#define TEST(MODE) test(MODE,#MODE)

int main(int,char**) {

  TEST(_MM_ROUND_NEAREST);
  TEST(_MM_ROUND_DOWN);
  TEST(_MM_ROUND_UP);
  TEST(_MM_ROUND_TOWARD_ZERO);

  return 0;
}

// Question: Is there a rounding mode such that (x-1.0f)+1.0f
// is a lossless transformation for all floats ?

import scala.annotation.strictfp

@strictfp def bad(x:Float) = ((x-1.0f)+1.0f != x)

val counterexample=Iterator.from(1).map(_.toFloat).find(bad).get

println(counterexample)

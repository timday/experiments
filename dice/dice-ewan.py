#!/usr/bin/env python

# Via Edinburgh Mathsjam FB page
#
# 1) Roll six dice.
# 2) If all are distinct, you win. End of game.
# 3) If all are equal, you lose. End of game.
# 4) Otherwise, pick up exactly the dice with repeated values, and re-roll.
#    (E.g, if the values are [1, 2, 3, 3, 4, 4], you would reroll the [3, 3, 4, 4] dice.)
# 5) Goto 2.
#
# Which is more likely, winning or losing?
#
# "Paul's rules": "are equal" means all dice the same
# "Ewan's rules": "are equal" means there are NO distinct dice

from collections import defaultdict
from itertools import *
import numpy as np

# This code implements Ewans rules:
# The only important state is the number of unique dice after the thow; these are retained.
# So a Markov chain with 7 states: 0 to 6 distinct dice inclusive.
# Paul's rules need (a little?) more attention to the configurations with zero-distinct
# (some of which become non-game-enders).

# Our dice labelled 0 to 5 inclusive
die=range(6)

# Possible outcomes from shaking n dice
def shake(n):
    dice=[die]*n
    return list(product(*dice))

# Count unique dice in a configuration
def countUnique(s):
    d=defaultdict(int)
    for i in s: d[i]+=1
    return len(filter(lambda p: p[1]==1,d.items()))

# Given a bunch of configurations, return proportions for number of distinct dice
def probabilities(configs):
    c=[countUnique(s) for s in configs]
    h=defaultdict(int)
    for i in c: h[i]+=1
    return np.array([h[i] for i in range(7)])/float(len(configs))

# Compute transition probabilities from n diverse dice
def transitions(n):
    if n==0:
        return np.array([1.0]+[0.0]*6) # Game lost
    elif n==6:
        return np.array([0.0]*6+[1.0]) # Game won
    else:
        outcomes=[list(s)+range(n) for s in shake(6-n)]
        return probabilities(outcomes)

def fmt(a):
    return np.array_repr(a,max_line_width=132,precision=6,suppress_small=True)

# Build transition matrix
T=np.zeros((7,7))
for col in range(7):
    T[:,col]=transitions(col)
print fmt(T)
print

# Intial state
state=probabilities(shake(6))

# Play!
for i in xrange(1000):
    print np.sum(state),fmt(state)
    state=np.dot(T,state)

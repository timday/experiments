#!/usr/bin/env python

# Via Edinburgh Mathsjam FB page
#
# 1) Roll six dice.
# 2) If all are distinct, you win. End of game.
# 3) If all are equal, you lose. End of game.
# 4) Otherwise, pick up exactly the dice with repeated values, and re-roll.
#    (E.g, if the values are [1, 2, 3, 3, 4, 4], you would reroll the [3, 3, 4, 4] dice.)
# 5) Goto 2.
#
# Which is more likely, winning or losing?
#
# "Paul's rules": "are equal" means all dice the same
# "Ewan's rules": "are equal" means there are NO distinct dice

from collections import defaultdict
from itertools import *
import numpy as np

# This code implements Paul's rules (see other file here for initial Ewan's rules implementation).
# The important state is the number of unique dice after the throw (which are the retained ones).
# However we must also break the zero-distinct state into "all equal" (which is game ending loss) and anything else.
# So a Markov chain with 8 states: 0 distinct and all equal, 0 distinct and various equal, and then 1 to 6 distinct dice inclusive.

# Our dice labelled 0 to 5 inclusive
die=range(6)

# Possible outcomes from shaking n dice
def shake(n):
    dice=[die]*n
    return list(product(*dice))

# Count unique dice in a configuration.
# If there are zero unique, actually return -1 to indidicate the all equal state
def countUnique(s):
    if len(set(s))==1:
        return -1
    else:
        d=defaultdict(int)
        for i in s: d[i]+=1
        return len(filter(lambda p: p[1]==1,d.items()))

# Given a bunch of configurations, return proportions for number of distinct dice
def probabilities(configs):
    c=[countUnique(s) for s in configs]
    h=defaultdict(int)
    for i in c: h[i]+=1
    return np.array([h[i] for i in range(-1,7)])/float(len(configs))

# Compute a transition matrix column
# NB Column index no longer corresponds to number of distinct dice due to split zero distinct state
def transitions(col):
    n=col-1
    if n==-1:
        return np.array([1.0]+[0.0]*7) # Game lost
    elif n==6:
        return np.array([0.0]*7+[1.0]) # Game won
    else:
        outcomes=[list(s)+range(n) for s in shake(6-n)]
        return probabilities(outcomes)

def fmt(a):
    return np.array_repr(a,max_line_width=132,precision=6,suppress_small=True)

# Build transition matrix
T=np.zeros((8,8))
for col in range(8):
    T[:,col]=transitions(col)
print fmt(T)
print

# Intial state
state=probabilities(shake(6))

# Play!
for i in xrange(1000):
    print np.sum(state),fmt(state)
    state=np.dot(T,state)

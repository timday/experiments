At <http://pages.stern.nyu.edu/~adamodar/New_Home_Page/datacurrent.html> I find a nice list of PE ratios by sector for Japan: <http://www.stern.nyu.edu/%7Eadamodar/pc/datasets/peJapan.xls>

At <http://markets.ft.com/research/markets/overview>:

* Canon PE: 16.69 from http://markets.ft.com/research/Markets/Tearsheets/Summary?s=7751:TYO

* Toshiba PE: not available at http://markets.ft.com/research/Markets/Tearsheets/Summary?s=6502:TYO but https://ycharts.com/companies/TOSBF/pe_ratio has 18.60 on 30th December (but quite volatile over the last year!)

* TMSC inferred PE: sold for $5.9 billion, but <http://www.wsj.com/articles/why-toshibas-big-deal-leaves-hard-work-undone-1457593308>.

* <http://asia.nikkei.com/magazine/20160317-THE-LAST-MILE/Business/Toshiba-sells-its-prized-medical-unit-to-Canon-to-save-itself> claims TMSC P/E ~25.

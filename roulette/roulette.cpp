#include <algorithm>
#include <array>
#include <functional>
#include <iostream>
#include <map>
#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
#include <tbb/scalable_allocator.h>
#include <tbb/task_scheduler_init.h>
#include <unordered_map>

typedef std::array<int,4> key_type;

// Hashmap is faster than oldschool map
//typedef std::map<key_type,double> map_type;
// Also tried memoising hash with key: no gain, marginally slower.

struct hasher {
  size_t operator()(const key_type& k) const {
    const std::hash<size_t> x;
    return x(size_t(k[0])) ^ x(size_t(k[1])<<8) ^ x(size_t(k[2])<<16) ^ x(size_t(k[3])<<24);
  }
};

typedef std::unordered_map<
  key_type,
  double,
  hasher,
  std::equal_to<key_type>,
  tbb::scalable_allocator<std::pair<key_type,double>>  // ~doubles performance on namche
  > map_type;

void add(map_type& m,int a,int b,int c,int d,double v) {
  if (v>0.0) {
    const key_type k={a,b,c,d};
    const map_type::iterator it=m.find(k);
    if (it==m.end()) {
      m.insert(std::make_pair(k,v));
    } else {
      it->second+=v;
    }
  }
}

// No measurable gain making this one a scalable allocator
typedef std::unordered_map<
  int,
  double
> pmap_type;

pmap_type fire(int n) {

  if (n==0 || n==1) {
    pmap_type r;
    r.insert(std::make_pair(n,1.0));
    return r;
  }

  map_type states;
  const key_type init={n,0,0,0};
  states.insert(std::make_pair(init,1.0));

  while (
    !std::all_of(
      states.begin(),
      states.end(),
      [](const std::pair<key_type,double>& p) {return (p.first[0]+p.first[1]==0);}
    )
  ) {
    map_type newstates;
    for (map_type::const_iterator it=states.begin();it!=states.end();++it) {
      const key_type& s=it->first;
      const double p=it->second;
      const int n=s[0]+s[1]+s[2]+s[3];
      const double k=p/(n-1);
      
      if (s[0]>1) {
        add(newstates,s[0]-1-1,s[1]+1,s[2]+1  ,s[3]    ,k*(s[0]-1   ));
        add(newstates,s[0]-1  ,s[1]  ,s[2]+1-1,s[3]+1  ,k*(s[2]     ));
        add(newstates,s[0]-1  ,s[1]  ,s[2]+1  ,s[3]    ,k*(s[1]+s[3]));
      } else if (s[0]==1) {
        add(newstates,s[0]-1  ,s[1]  ,s[2]+1-1,s[3]+1  ,k*(s[2]     ));
        add(newstates,s[0]-1  ,s[1]  ,s[2]+1  ,s[3]    ,k*(s[1]+s[3]));
      } else {
        add(newstates,s[0]    ,s[1]-1,s[2]-1  ,s[3]+1+1,k*(s[2]       ));
        add(newstates,s[0]    ,s[1]-1,s[2]    ,s[3]+1  ,k*(s[1]-1+s[3]));
      }
    }
    states.swap(newstates);
  }
  
  pmap_type r;
  for (map_type::const_iterator it=states.begin();it!=states.end();++it) {
    r.insert(std::make_pair(it->first[2],it->second));
  }
  std::cerr << ".";
  return r;
}

class psurvivor {
public:
  psurvivor(int N)
  :_N(N)
  ,_fires(N+1)
  {
    tbb::parallel_for(
      tbb::blocked_range<int>(0,N+1),
      [this](const tbb::blocked_range<int>& r) {
        for (int i=r.begin();i!=r.end();++i) {
          _fires[i]=fire(i);
        }
      }
    );
  }

  double operator()(int n) {
    if (n==0) {
      return 0.0;
    } else if (n==1) {
      return 1.0;
    } else {
      pmap_type::const_iterator it=_p.find(n);
      if (it!=_p.end()) {
        return it->second;
      } else {
        double r=0.0;
        for (it=_fires[n].begin();it!=_fires[n].end();++it) {
          r+=(*this)(it->first)*it->second;
        }
        _p[n]=r;
        return r;
      }
    }
  }
  
private:
  const int _N;
  std::vector<pmap_type> _fires;
  pmap_type _p;
};

int main(int,char**) {

  tbb::task_scheduler_init init;

  const int N=3000;

  psurvivor p(N);
  std::cout.precision(15);
  for (int i=1;i<=N;++i) {
    std::cout << i << " " << p(i) << std::endl;
  }

  return 0;
}

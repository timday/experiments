#!/usr/bin/env python

import csv
import math
import numpy as np
import matplotlib.pyplot as plt

reader=csv.reader(file('results.txt','r'),delimiter=' ')

def process(row):
    return np.array(map(float,row))

results=np.array([process(row) for row in reader])

minima=[i for i in xrange(1,results.shape[0]-1) if i>10 and results[i-1,1]>results[i,1] and results[i,1]<results[i+1,1]] 
maxima=[i for i in xrange(1,results.shape[0]-1) if i>10 and results[i-1,1]<results[i,1] and results[i,1]>results[i+1,1]] 

plt.hlines(y=results[minima,1],xmin=results[minima,0],xmax=results[-1,0],color='b',alpha=0.5,label='minima/maxima')
plt.hlines(y=results[maxima,1],xmin=results[maxima,0],xmax=results[-1,0],color='b',alpha=0.5)

def fraclog(x):
    y=math.log(x)
    return y-math.floor(y)

marks=[i for i in results[1:,0] if fraclog(i)<fraclog(i-1)]

plt.vlines(marks,0.0,1.0,color='g',alpha=0.5,label='fractional log wraps')

plt.scatter(results[:,0],results[:,1],color='r',label='p(sole survivor)',marker='.')
plt.xlim(10,results[-1,0])
plt.ylim(0.45,0.55)
#plt.ylim(0.0,1.0)
plt.xlabel('Initial n')
plt.xscale('log')
plt.legend()
plt.title('Group Russian Roulette')
plt.show()

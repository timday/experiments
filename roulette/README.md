Group Russian Roulette
======================

See <problem.jpg> for problem posted to Edinburgh Mathsjam by Paul.

Some discussion at <http://www.wilmott.com/messageview.cfm?catid=26&threadid=88329>.
Second-to-last post on 1st page there suggests a more direct calculation of p(n->k) for a single round.  
Links to <http://arxiv.org/PS_cache/cs/pdf/0502/0502014v1.pdf> re convergence.

EC2
---

    sudo aptitude update
    sudo aptitude full-upgrade
    sudo aptitude install mercurial emacs scala time less make g++ libtbb-dev
    mkdir project
    cd project
    hg clone https://timday@bitbucket.org/timday/experiments
    cd experiments/roulette
    nohup make &

    

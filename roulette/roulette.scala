// Compute outcome of a single round with n players
// yields probabilties of various numbers of survivors
def fire(n:Int):Map[Int,Double] = {

  type Key=(Int,Int,Int,Int)
  // State is (a,b,c,d)
  //   a: number to shoot and untagged
  //   b: number to shoot and tagged
  //   c: number already shot and untagged
  //   d: number already shot and tagged

  // Combine probabilities
  def merge(s:Iterable[(Key,Double)]):Map[Key,Double] = {
    val r=scala.collection.mutable.Map[Key,Double]().withDefaultValue(0.0)
    s.filter(_._2>0.0).foreach(
      p=>{
	r.update(p._1,p._2+r(p._1))
      }
    )
    r.toMap
  }

  // Advance state until all fired; a+b=0, c+d=n
  def advance(states:Map[Key,Double]):Map[Key,Double] = {
    if (states.forall(p=>(p._1._1+p._1._2==0))) {
      states
    } else {
      advance(
	merge(
	  states.toIterable.flatMap(
	    s=>{
	      val ((a,b,c,d),p)=s
	      val n=a+b+c+d
              val k=p/(n-1)
	      (
                if (a>1) {
		  Iterable(
		    (a-1-1,b+1,c+1  ,d  ) -> k*(a-1),   // Shoots someone yet to fire and untagged
		    (a-1  ,b  ,c+1-1,d+1) -> k*(c  ),   // Shoots someone already fired and untagged
		    (a-1  ,b  ,c+1  ,d  ) -> k*(b+d)    // No effect: fires at someone already tagged
		  )
		} else if (a==1) {
                  // No option here to shoot someone else yet to fire and untagged
		  Iterable(
		    (a-1  ,b  ,c+1-1,d+1) -> k*(c  ),   // Shoots someone already fired and untagged
		    (a-1  ,b  ,c+1  ,d  ) -> k*(b+d)    // No effect: fires at someone already tagged
		  )           
		} else {
                  assert(a==0)  // No option here to shoot someone yet to fire and untagged
		  assert(b>0)
		  Iterable(
		    (a  ,b-1  ,c-1,d+1+1) -> k*(c    ),   // Shoot someone already fired and untagged
		    (a  ,b-1  ,c  ,d+1  ) -> k*(b-1+d)    // No effect fires at someone already tagged
		  )
		}
	      )
	    }
	  )
	)
      )
    }
  }

  System.err.print(".")

  // and read out probabilities for various n,c (number of survivors)
  advance(Map((n,0,0,0)->1.0)).map(p=>(p._1._3,p._2))
}

val N=250

// Tabulate fires for various n.  Parallelize.
val fires=Vector()++(0 to N).par.map(fire)

// Investigate simpler expression


// Now consider multiple rounds and probablility of single survivor
object ps {
  
  // DP it
  val memo=scala.collection.mutable.Map[Int,Double]()

  def compute(n:Int):Double = {
    if (n==0) {
      0.0
    } else if (n==1) {
      1.0
    } else {
      fires(n).map(s=>ps(s._1)*s._2).foldLeft(0.0)(_+_)
    }
  }

  def apply(n:Int):Double = memo.getOrElseUpdate(n,compute(n))
}

// Tabulate sole survivor probability
(1 to N).toIterator.map(
  i=>(i,ps(i))
).foreach(
  s=>println(s._1+" "+s._2)
)

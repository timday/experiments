#!/usr/bin/env python

# For https://moneyforums.citywire.co.uk/yaf_postsm80438_Momentum-Factor-ETF.aspx#post80438

import csv
import matplotlib.pyplot as plt

ignoredTickers=set(['AUD','CAD','CHF','DKK','EUR','GBP','HKD','ILS','JPY','NOK','NZD','SEK','SGD','USD','MARGIN_EUR','-'])

def main():

    tickerToName={}
    tickerToSWDAWeight={}
    with open('SWDA_holdings-20190407.csv') as csvfile:
        reader=csv.reader(csvfile)
        reader.next()
        reader.next()
        reader.next()
        for row in reader:
            if len(row)>=13:
                if not row[0] in ignoredTickers:
                    ticker='{} ({}, {})'.format(row[0],row[11],row[12])
                    if ticker in tickerToName:
                        raise ValueError('Duplicate ticker "{}"'.format(ticker))
                    tickerToName[ticker]=row[1]
                    tickerToSWDAWeight[ticker]=float(row[3])

    print 'SWDA: {} holdings, total weight {}'.format(len(tickerToSWDAWeight),sum(x for x in tickerToSWDAWeight.values()))

    tickerToIWFMWeight={}
    with open('IWFM_holdings-20190407.csv') as csvfile:
        reader=csv.reader(csvfile)
        reader.next()
        reader.next()
        reader.next()
        for row in reader:
            if len(row)>=13:
                if not row[0] in ignoredTickers:
                    ticker='{} ({}, {})'.format(row[0],row[11],row[12])
                    if not ticker in tickerToName:
                        print 'Unexpected ticker {} in IWFM holdings (weight {})'.format(ticker,row[3])
                    else:
                        tickerToIWFMWeight[ticker]=float(row[3])

    print 'IWFM: {} holdings, total weight {}'.format(len(tickerToIWFMWeight),sum(x for x in tickerToIWFMWeight.values()))

    print 'IWFM holdings {:.1f}% of SWDA'.format(sum(tickerToSWDAWeight[ticker] for ticker in tickerToIWFMWeight.keys()))
    print

    tickerToFactor={}
    for ticker in tickerToIWFMWeight.keys():
        if tickerToSWDAWeight[ticker]>0.0:
            tickerToFactor[ticker]=tickerToIWFMWeight[ticker]/tickerToSWDAWeight[ticker]
        else:
            tickerToFactor[ticker]=1.0

    print 'Top weighted IWFM stock factors'
    for it in sorted(tickerToIWFMWeight.keys(),key=lambda x: tickerToIWFMWeight[x],reverse=True)[:20]:
        print '{:40s}: {:30s}: x{:>4.1f}'.format(tickerToName[it],it,tickerToFactor[it])
    print

    print 'All IWFM tickers, ranked by factor'
    for it in sorted(tickerToFactor.items(),key=lambda x: x[1],reverse=True):
        print '{:40s}: {:30s}: x{:>4.1f}'.format(tickerToName[it[0]],it[0],it[1])
    print

    xs=[tickerToSWDAWeight[ticker] for ticker in tickerToIWFMWeight.keys()]
    ys=[tickerToIWFMWeight[ticker] for ticker in tickerToIWFMWeight.keys()]
    cs=[tickerToFactor[ticker] for ticker in tickerToIWFMWeight.keys()]

    topSWDA=[x[0] for x in sorted(tickerToSWDAWeight.items(),key=lambda x: x[1],reverse=True)][:20]
    topIWFM=[x[0] for x in sorted(tickerToIWFMWeight.items(),key=lambda x: x[1],reverse=True)][:20]
    topFactor=[x[0] for x in sorted(tickerToFactor.items(),key=lambda x: x[1],reverse=True)][:20]
    tops=set(topSWDA).union(set(topIWFM)).union(set(topFactor))

    for ticker in sorted(list(tops),key=lambda x: tickerToSWDAWeight[x],reverse=True):
        print '{:40s}: {:30s}: {:>4.2f}%'.format(tickerToName[ticker],ticker,tickerToSWDAWeight[ticker])

    topMissingSWDA=[x for x in sorted(tickerToSWDAWeight.items(),key=lambda x: x[1],reverse=True) if not x[0] in tickerToIWFMWeight][:20]
    print
    print 'Top missing SWDA tickers'
    for it in topMissingSWDA:
        print '{:40s}: {:30s}: {:>4.2f}%'.format(tickerToName[it[0]],it[0],it[1])

    txts=[(ticker.split('(')[0].strip(),tickerToName[ticker],tickerToSWDAWeight[ticker],tickerToIWFMWeight[ticker]) if ticker in tops else None for ticker in tickerToIWFMWeight.keys()]
    plt.plot([0.0,10.0],[0.0,10.0],color='#888888')
    plt.scatter(xs,ys,c=cs,edgecolors='none',cmap=plt.get_cmap('rainbow'))
    plt.colorbar()
    infos=[]
    for i in xrange(len(txts)):
        if txts[i]!=None:
            plt.gca().text(xs[i],ys[i],txts[i][0],{'ha':'left','va':'bottom'},rotation=45.0)
            infos.append(txts[i])
    info='\n'.join('{}: {} ({:.1f}% vs. {:.1f}%: x{:.1f})'.format(it[0],it[1],it[3],it[2],it[3]/it[2]) for it in sorted(infos,key=lambda x: x[0],reverse=False))
    print
    print 'Labelling interesting tickers:'
    print info
    plt.xscale('symlog',linthreshx=0.1)
    plt.yscale('symlog',linthreshy=0.1)
    plt.xlabel('SWDA weighting % (linear below 0.1%)')
    plt.ylabel('IWFM weighting % (linear below 0.1%)')
    plt.xlim(xmin=0,xmax=max(10.0,max(xs)+1.0))
    plt.ylim(ymin=0,ymax=max(10.0,max(ys)+1.0))
    plt.title('IWFM weighting vs. SWDA weighting\nColoured by factor')
    plt.text(plt.gca().get_xlim()[1]*0.1,0.01,info,ha='left',va='bottom',size=6)  # size='xx-small'
    plt.show()
        
if __name__ == '__main__':
    main()
